package testPackage;

import java.util.List;
import javax.persistence.*;

@Entity
public class Equipe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_equipe")
	private int id;
	private String nom;
	
	@OneToMany
	private List<Etudiant> EtudiantList;
	
	public Equipe(){
		this.nom = "Equipe Loosers!!";
	}
	
	public Equipe(String n){
		this.nom = n;
	}
	
	public String getNom(){
		return nom;
	}
	
	public void setNom(String n){
		this.nom = n;
	}
	
	public int getId(){
		return id;
	}
	
	public void ajoutEtudiant(Etudiant e){
		this.EtudiantList.add(e);
	}
	
	@OneToMany (cascade = {CascadeType.PERSIST}, fetch=FetchType.LAZY)
	public List<Etudiant> getEtudiantList() {
		return this.EtudiantList;
	}

	public void setEtudiantList(List<Etudiant> el) {
		this.EtudiantList = el;
	}
	
}
