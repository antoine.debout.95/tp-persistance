package testPackage;

import java.util.List;
import javax.persistence.*;

@Entity
public class Projet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_projet")
	private int id;
	private String nom;
	
	@OneToMany
	private List<Etudiant> EtudiantList;
	
	public Projet()
	{
		this.nom = "Awesome Project";
	}
	
	public Projet(String n)
	{
		this.nom = n;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String n) {
		this.nom = n;
	}

	public int getId() {
		return id;
	}
	
	@OneToMany (cascade = {CascadeType.PERSIST}, fetch=FetchType.LAZY)
	public List<Etudiant> getEtudiantList() {
		return this.EtudiantList;
	}

	public void setEtudiantList(List<Etudiant> el) {
		this.EtudiantList = el;
	}
}
