package testPackage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Id;
import java.util.List;



@Entity
public class Ecole {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id_ecole")
	private int id;
	@OneToMany
	@JoinTable(name="ecole", joinColumns = @JoinColumn(name="id_ecole"), inverseJoinColumns = @JoinColumn(name = "id_ecole"))
	private List<Etudiant> EtudiantList;
	private String nomEcole;
	
	public Ecole(){
		this.nomEcole = "Ecole Buissonière";
	}
	
	public Ecole(String n){
		this.nomEcole = n;
	}
	
	public int getId(){
		return id;
	}
	
	public String getNomEcole(){
		return nomEcole;
	}
	
	public void setNomEcole(String n){
		this.nomEcole = n;
	}
	
	@OneToMany(cascade = {CascadeType.PERSIST}, fetch=FetchType.LAZY)
	public List<Etudiant> getEtudiantList(){
		return this.EtudiantList;
	}
	
	public void setEtudiantList(List<Etudiant> l) {
		this.EtudiantList = l;
	}
}
