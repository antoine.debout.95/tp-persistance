package testPackage;

import testPackage.Etudiant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;


public class EtudiantDAO {
	public static void main(String [] args){
		
		//INSTANCE D'UN NOUVEL ENTITY MANAGER
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Tpersistance");
		EntityManager em = emf.createEntityManager();
		//DEBUT DE LA TRANSACTION
		em.getTransaction().begin();
		
		//INSTANCIATION D'UN PREMIER ELEVE
		Etudiant eleve = new Etudiant();
		eleve.setAge(20);
		eleve.setNom("VAUQUIS");
		eleve.setPrenom("Thomas");
		eleve.setAdress("C'est la que j'habite");
		System.out.println("Nom : " + eleve.getNom() +" - Pr�nom : " + eleve.getPrenom() + " - Age : " + eleve.getAge());
		//AJOUT EN MEMOIRE DU PREMIER ELEVE
		em.persist(eleve);
		
		//INSTANCIATION D'UN SECOND ELEVE
		Etudiant jacquie = new Etudiant();
		jacquie.setAge(69);
		jacquie.setNom("MICHEL");
		jacquie.setPrenom("Jacquie");
		jacquie.setAdress("Dans la cave");
		//MISE EN MEMOIRE DU SECOND ETUDIANT
		em.persist(jacquie);
		
		//INSTANCIATION D'UNE EQUIPE
		Equipe teamIndesctructibles = new Equipe();
		teamIndesctructibles.setNom("Jack Jack");
		//MISE EN MEMOIRE DE L'EQUIPE
		em.persist(teamIndesctructibles);
		
		 //CREATION DE LISTES POUR LES DIFFERENTES CLASSES (C & D)
		 List<Etudiant> EtudiantList = new ArrayList<Etudiant>();
		 List<Projet> ProjetList = new ArrayList<>();
		 List<Equipe> EquipeList = new ArrayList<>();
		 
		
		//AJOUT D'UN ETUDIANT A LA LISTETUDIANT
		EtudiantList.add(jacquie);
		
		//AJOUT DE LA LIST A L'EQUIPE
		EquipeList.add(teamIndesctructibles);
		teamIndesctructibles.setEtudiantList(EtudiantList);
		
		//jacquie.setTeam(teamIndesctructibles)
		
		//AFFICHAGE DE LA LISTE D'ETUDIANT (B)
		Query query = em.createQuery("SELECT e FROM Etudiant e");
		List<Etudiant> list =  query.getResultList();
//		
		Query q = em.createQuery("SELECT e FROM Projet e");
		List<Projet> list1 =  q.getResultList();
		
		Query q2 = em.createQuery("SELECT e FROM Equipe e");
		List<Equipe> list2 = q2.getResultList();
		
		for (int i = 0; i < list.size() && i < list2.size(); i++) {
			System.out.println("Prenom : " + list.get(i).getPrenom() + " Nom : " + list.get(i).getNom() + " Age : " + list.get(i).getAge() + " Adresse : " + list.get(i).getAdresse());
			System.out.println("Nom projet: " + list1.get(i).getNom());
			System.out.println("Nom equipe : " + list2.get(i).getNom());
		
		}
		//FIN AFFICHAGE DE LA LISTE D'ETUDIANTS (B)
		
		//LISTE ELEVES
		List<Etudiant> EtudiantList = new ArrayList<>();	
		EtudiantList.add(eleve);
		EtudiantList.add(jacquie);
		//FIN LIST ELEVE
		
		//AFFICHAGE DE LISTE ETUDIANT AVEC LEUR ADRESSE
		
		Query q3 = em.createQuery("SELECT e FROM Etudiant e");
		List<Etudiant> list =  q3.getResultList();
		
		System.out.println("nb" + list.size());
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Prenom : " + list.get(i).getPrenom() + " Nom : " + list.get(i).getNom() + " Age : " + list.get(i).getAge() + " Adresse : " + list.get(i).getAdress());
		}
		
		
		//AFFICHAGE DE TOUT LES ETUDIANTS (A)
		
		List<Etudiant> list = em.createQuery("SELECT e FROM Etudiant e").getResultList();
		Iterator it = list.iterator();
				
		while(it.hasNext()){
			Etudiant e = (Etudiant)it.next();
			System.out.println("Nom = " + e.getNom() + " Prenom = " + e.getPrenom() + " Age = " + e.getAge());
		}
		// FIN (A)
		
		
		
		
		//COMMIT VERS LA BDD
		em.getTransaction().commit();
		

//************************************************************************************************************************
//***************************************************   AFFICHAGE DES INFORMATIONS DE TOUS LES ETUDIANTS  ****************
//************************************************************************************************************************		
		List<Etudiant> listEtudiant = em.createQuery("SELECT e From Etudiant e").getResultList();
		Iterator i = listEtudiant.iterator();
		while(i.hasNext()){
			Etudiant e = (Etudiant) i.next();
			System.out.println("Nom : " + e.getNom() +" - Pr�nom : " + e.getPrenom() + " - Age : " + e.getAge());
		}
//************************************************************************************************************************
//************************************************   FIN AFFICHAGE DES INFORMATIONS DE TOUS LES ETUDIANTS  ****************
//************************************************************************************************************************	
		
		em.close();
		emf.close();
		
	}
}
