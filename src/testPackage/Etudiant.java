package testPackage;


import java.util.List;

import javax.persistence.*;

@Entity
public class Etudiant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long etu_id;
	@Column (name="nom", nullable= false)
	private String nom;
	@Column (name="prenom", nullable= false)
	private String prenom;
	@Column (name="age", nullable= false)
	private int age;
	
	private Equipe team;
	private String adress;
	
	@OneToMany
	@JoinTable(name = "Etudiant", joinColumns = @JoinColumn (name = "id_etudiant"), inverseJoinColumns = @JoinColumn(name = "id_etudiant"))
	private List<Projet> ProjetList;
	
	
	public String getNom() {
		return nom;
	}
	
	public Etudiant(){
		this.nom = new String();
		this.prenom = new String();
		this.age = 0;
		this.adress = "69 rue Jacquie Tunning";
		
	}
	
	public Etudiant(String nom, String prenom, int age, String a) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.adress = a;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getAdress() {
		return adress;
	}

	public void setAdress(String a) {
		this.adress = a;
	}
	
	public Equipe getTeam() {
		return team;
	}

	public void setTeam(Equipe t) {
		this.team = t;
		t.ajoutEtudiant(this);
	}
	
	@ManyToMany (cascade = {CascadeType.PERSIST}, fetch=FetchType.LAZY)
	public List<Projet> getProjet() {
		return this.ProjetList;
	}

	public void setProjet(List<Projet> pl) {
		this.ProjetList = pl;
	}
	
}
